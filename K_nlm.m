% ------------------------------------------------
function Knlm = K_nlm(n,l,m,kk,BB,r_i,rayon,theta)
% ------------------------------------------------
% rotational acoustic kernels for a spherical shell (see Triana et al, 2014)
%   n : mode radial index
%	l : mode angular degree
%	m : mode azimuthal order
%	r_i : dimensionless inner sphere radius (used to compute I_nlm)
%   rayon : <Nr x 1> column vector of dimensionless radius from inner to outer shell 
%	theta : <1 x Nt> line vector of colatitudes
%   kk : <n_max x l_max> matrix of mode wavenumbers (m-degenerate)
%   BB : <n_max x l_max> matrix of mode B ratios (m-degenerate)
%
%	Knlm : <Nr x Nt> matrix of kernel K_{nlm}
%
% 190408 HCN solve NaN issues with asymptotics for r=0, and theta=0,pi
% 180917 HCN : suppress rayon from input arguments of Int_nlm
% *** NB : NaN present ***
% 171019 HCN from Santiago's original kernel2.m
%
% get (n,l) mode data (they are all m-degenerate)
k1=kk(n+1,l+1);
B1=BB(n+1,l+1);
%
% setup the output matrix
Nt = length(theta);
Nr = length(rayon);
Knlm = zeros(Nr,Nt);

% Normalization I_{nlm} in the fluid domain
I_nlm = Int_nlm(l,k1,B1,r_i,1);

% Displacement functions
xir = xi_r(l,k1,B1,rayon);
xih = xi_h(l,k1,B1,rayon);

% Now down to business calculating the kernel
ss = sin(theta);
cc = cos(theta);
p = P_lm(l,m,theta);
%q = Q_lm(l,m,theta); % = d_P_lm
q = d_P_lm(l,m,theta);

% <Nr x Nt> matrix terms inside the braces :    
term1 = xir.^2 * p.^2.*ss;
%
vicious_term = m^2*p.^2./ss;
%
is_nul = find(theta==0); % check if zero is present in vector theta
if ~isempty(is_nul) % theta=0 found
    vicious_term(is_nul) = 0; % zero for all m for theta=0 (even m=0 ?)
end
%
is_nul = find(theta==pi); % check if pi is present in vector theta
if ~isempty(is_nul) % theta=pi found
    vicious_term(is_nul) = 0; % zero for all m for theta=pi (even m=0 ?)
end
%
term2 = xih.^2 * (q.^2.*ss + vicious_term - 2*p.*q.*cc);
%
term3 = -2*xir.*xih * p.^2.*ss;
% <Nr x Nt> kernel matrix
Knlm = (rayon * ones(size(ss))) .* (term1 + term2 + term3) / I_nlm;