% -----------------------------------------------------------------------
function C_nl = Ledoux_coefficient(rf, kk, BB);
% -----------------------------------------------------------------------
% computes the Ledoux coefficient C_nl (equation 3.361 of Asteroseismology by Aerts et al)
% that produces the splitting -m C_nl Omega of mode _nS_l^m due to the Coriolis force
% when the mode is observed in the rotating frame of the shell in solid body rotation Omega
%
% inputs :
%	rf(1) = radius of the inner sphere (0 if none) (m)
%	rf(2) = radius of the outer sphere (m)
%	kk(nn+1,ll+1) = 'k' of mode nnS_ll (noted z in Rand and DiMaggio)
%	BB(nn+1,ll+1) = factor of the y_l part for mode nnS_ll
%
% Note : [n_max+1,l_max+1] = size(kk);
%
% outputs :
%	C_nl(nn+1,ll+1) = Ledoux coefficient
%
% 150903 HCN créé
% 170809 HCN : corrige gros bug : les bornes d'intégration en rayon doivent être
% admimensionnés par rf(2)...
% 170901 HCN : encore un gros bug vu par Sylvie : dj_n(k*r)/dr = k*dj_n(u)/du contrairement a ce que j'avais
% 171017 HCN : j'exclus le mode 0S0 qui pose problème
% 190730 HCN : slight rewriting using Int_nlm
%
disp('... computing Ledoux coefficients C_nl ...');
%
[n_max,l_max] = size(kk);
n_max=n_max-1;
l_max=l_max-1;
%
C_nl(1,1) = NaN; % exclude mode 0S0
%
for nn = 0:n_max
%	disp(['n = ' int2str(nn)]);
% exclude mode 0S0
	if (nn==0)
		l_min=1;
	else
		l_min=0;
	end
%
	for ll = l_min:l_max
%		disp(['     l = ' int2str(ll)]);
		B = BB(nn+1,ll+1) ;
		k = kk(nn+1,ll+1) ;
% integrate function numerateur from rf(1) to rf(2)
%		num = integral(@(r) numerateur(ll,k,B,r),rf(1)/rf(2),1);
		num = quadgk(@(r) numerateur(ll,k,B,r),rf(1)/rf(2),1);
% integrate function denominateur = Int_nlm
%		denom = integral(@(r) denominateur(ll,k,B,r),rf(1)/rf(2),1) ;
        denom = Int_nlm(ll,k,B,rf(1)/rf(2),1);
		C_nl(nn+1,ll+1) = num/denom ;
	end % l loop
end % n loop
%
end
%% =================== fonctions pour integration =====================
% -----------------------------------------------------------------------
function f=numerateur(l,k,B,r) 
% -----------------------------------------------------------------------
% integrand of the upper term of the C_nl Ledoux coefficient
f = (2*xi_r(l,k,B,r).*xi_h(l,k,B,r) + xi_h(l,k,B,r).^2).*r.^2 ;
end
% -----------------------------------------------------------------------
%function f=denominateur(l,k,B,r) 
% -----------------------------------------------------------------------
% integrand of the lower term of the C_nl Ledoux coefficient
%f = (xi_r(l,k,B,r).^2 + l*(l+1)*xi_h(l,k,B,r).^2).*r.^2 ;
%end
% NB : fonctions xi et Bessel sphériques et derivees dans folder acoustic_library
% ============================================================================