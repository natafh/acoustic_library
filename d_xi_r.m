% --------------------------
function f = d_xi_r(l,k,B,r) 
% --------------------------
% radial derivative of the radial displacement function xi_r = dR(r)/dr 
% (see Triana et al, 2014)
% 	l : angular degree
%	k : mode wave number
%	B : ratio of y_l to j_l Bessel functions for this mode
%	r : vector of dimensionless radius
% 170901 HCN corrige bug : d^2j_n(k*r)/dr^2 = k^2*d^2j_n(u)/du^2 en u=k*r
f = k^2*d2_spherical_bessel_j(l,k*r) + B * k^2*d2_spherical_bessel_y(l,k*r) ;