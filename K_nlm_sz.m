% ----------------------------------------------------------------------
function [Knlm_sz, rayon, theta] = K_nlm_sz(n,l,m,kk,BB,r_i,s_radius,zz)
% ----------------------------------------------------------------------
% rotational acoustic kernels for a spherical shell (see Triana et al, 2014)
% expressed in (s,z) coordinates for z-inversion
% outputs vectors of same dimension as z-coordinate zz
% s_radius should be a scalar
%
% input :
%   n : mode radial index
%	l : mode angular degree
%	m : mode azimuthal order
%	r_i : dimensionless inner sphere radius (used to compute I_nlm)
%   s_radius : scalar dimensionless cylindrical radius
%	zz : <1 x Nz> line vector of z coordinate
%   kk : <n_max x l_max> matrix of mode wavenumbers (m-degenerate)
%   BB : <n_max x l_max> matrix of mode B ratios (m-degenerate)
%
% output :
%	Knlm_sz : <1 x Nz> matrix of kernel K_{nlm}
%   rayon : <1 x Nz> line vector of spherical radius
%   theta : <1 x Nz> line vector of colatitude
%
% 190903 HCN : corrected several of the '*' into '.*' (how did it work earlier ???)
% 180917 HCN : adapted from K_nlm but scalar product instead of vector product
%
% setup the output matrix
Nz = length(zz);
Knlm_sz = zeros(1,Nz);

% return 0 for s_radius = 0 to avoid NaN problems
if (s_radius==0)
    rayon = zeros(1,Nz);
    theta = zeros(1,Nz);
    return
end
%
rayon = sqrt(zz.^2 + s_radius^2); % <1 x Nz> line vector of spherical radius
theta = atan2(s_radius,zz); % <1 x Nz> line vector of colatitude
%
% get (n,l) mode data (they are all m-degenerate)
k1=kk(n+1,l+1);
B1=BB(n+1,l+1);
%
% Normalization I_{nlm} in the fluid domain
I_nlm = Int_nlm(l,k1,B1,r_i,1);

% Displacement functions
xir = xi_r(l,k1,B1,rayon); % <1 x Nz>
xih = xi_h(l,k1,B1,rayon); % <1 x Nz>

% Now down to business calculating the kernel
ss = sin(theta); % <1 x Nz>
cc = cos(theta); % <1 x Nz>
p = P_lm(l,m,theta); % <1 x Nz>
q = d_P_lm(l,m,theta); % <1 x Nz>

% <1 x Nz> terms inside the brace :    
term1 = xir.^2 .* p.^2.*ss;
%
vicious_term = m^2*p.^2./ss;
%
is_nul = find(theta==0); % check if zero is present in vector theta
if ~isempty(is_nul) % theta=0 found
    vicious_term(is_nul) = 0; % zero for all m for theta=0 (even m=0 ?)
end
%
is_nul = find(theta==pi); % check if pi is present in vector theta
if ~isempty(is_nul) % theta=pi found
    vicious_term(is_nul) = 0; % zero for all m for theta=pi (even m=0 ?)
end
%
term2 = xih.^2 .* (q.^2.*ss + vicious_term - 2*p.*q.*cc);
%
term3 = -2*xir.*xih .* p.^2.*ss;
% <1 x Nz> kernel vector
Knlm_sz = rayon .* (term1 + term2 + term3) / I_nlm;