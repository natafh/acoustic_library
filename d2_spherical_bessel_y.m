% -------------------------------------
function f = d2_spherical_bessel_y(n,x)
% -------------------------------------
% second derivative of the spherical Bessel function of the second kind
% d2/dx2 y(n,x)
% 170810 HCN
f = sqrt(pi./(2*x.^3)).*(bessely(n+1/2,x).*(n*(n-1)./x - x) + 2*bessely(n+3/2,x));