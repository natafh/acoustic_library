% -----------------------------------------------------------------------
function Acc = acceleration_coefficient(rho_f, rf, kk, BB)
% -----------------------------------------------------------------------
% compute the Acceleration/Wall Pressure ratio for mode nnS_ll (m/s^2/Pa)
% Acceleration/Wall pressure ratio from equation (15) of Rand and DiMaggio (1967)
%
% inputs :
%   rho_f = (kg m-3) fluid density
%	rf(1) = (m) radius of the inner sphere (0 if none)
%	rf(2) = (m) radius of the outer sphere
%	kk(nn+1,ll+1) = 'k' of mode nnS_ll (noted z in Rand and DiMaggio)
%	BB(nn+1,ll+1) = factor of the y_l part for mode nnS_ll
%
% Note : [n_max+1,l_max+1] = size(kk);
%
% outputs :
%	Acc(nn+1,ll+1) = (m/s^2/Pa = m¨2/kg) acceleration coefficient
%
% 190117 HCN transféré depuis elastic_spherical_shell_modes
%  *** en construction ***
%
disp('... computing acceleration coefficients Acc ...');
%
[n_max, l_max] = size(kk);
n_max=n_max-1;
l_max=l_max-1;
%
for ll=0:l_max
    l = ll+1;
    clear z
    z(1:n_max+1) = kk(1:n_max+1,l); % the computed k (or z) of all modes with ll angular number
%
    if (rf(1) == 0) % no inner sphere
% full fluid sphere with rigid or elastic shell
        Acc(1:n_max+1,l) = -(z(1:n_max+1)/rho_f/rf(2)) .*(d_spherical_bessel_j(ll, z(1:n_max+1)))./(spherical_bessel_j(ll, z(1:n_max+1)));
%
    else % there is an inner sphere
% NB : need to transpose BB matrix for .product
        Acc(1:n_max+1,l) = -(z(1:n_max+1)/rho_f/rf(2)) .*(d_spherical_bessel_j(ll, z(1:n_max+1)) + BB(1:n_max+1,l)'.*d_spherical_bessel_y(ll, z(1:n_max+1)))./(spherical_bessel_j(ll, z(1:n_max+1)) + BB(1:n_max+1,l)'.*spherical_bessel_y(ll, z(1:n_max+1)));
    end
end