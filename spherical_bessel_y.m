% ----------------------------------
function f = spherical_bessel_y(n,x)
% ----------------------------------
% spherical Bessel function of the second kind
% y(n,x) = sqrt(2/pi x) * Y(n + 1/2, x)
f = sqrt(pi/2)*bessely(n+1/2,x)./sqrt(x);