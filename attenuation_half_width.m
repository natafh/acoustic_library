% --------------------------------------------------------------------------
function [g_nl, delta_f_nl] = attenuation_half_width(rf, kk, ff, Gruneisen, Prandtl, nu_f, nu_b)
% --------------------------------------------------------------------------
% computes the attenuation half-widths g_nl of modes (n,l), following the approach
% of Moldover et al (1986). Also see Trusler's book (1991).
% delta_f_nl gives the correction to the frequencies of the ideal modes.
%
% 190125 HCN : correct \nu_b/2 of Moldover (1986) into \nu_b of others...
% 190123 HCN : add the bulk viscosity nu_b : can vary widely (Cramer, 2012) ! 
% 171016 HCN
disp('... computing attenuation half-widths g_nl ...');
%
kappa = nu_f/Prandtl;
%
[n_max, l_max] = size(kk);
n_max=n_max-1;
l_max=l_max-1;
%
for nn = 0:n_max
%	disp(['n = ' int2str(nn)]);
	for ll = 0:l_max
%		disp(['     l = ' int2str(ll)]);
		k = kk(nn+1,ll+1) ;
		f = ff(nn+1,ll+1) ;
%
% bulk dissipation (~10^-3 for air. Can be much larger)
% 190125 HCN : correct \nu_b/2 of Moldover (1986) into \nu_b of others...
		g_bulk = k^2/(4*pi*rf(2)^2)*(4*nu_f./3. + nu_b +(Gruneisen-1)*kappa);
%
% boundary layer dissipation (~0.3 or more)
		delta_t = sqrt(kappa/pi/f); % (m) thermal boundary layer thickness
		delta_v = sqrt(nu_f/pi/f); % (m) viscous boundary layer thickness
		g_boundary = f/rf(2)./2 * ((Gruneisen-1)*delta_t + ll*(ll+1)/k^2*delta_v)./(1 - ll*(ll+1)/k^2);
%
% add boundary layer on the inner sphere if present
		if (rf(1)~=0)
		    g_boundary = g_boundary + f/rf(1)./2 * ((Gruneisen-1)*delta_t + ll*(ll+1)/k^2*delta_v)./(1 - ll*(ll+1)/k^2);			
		end
		g_nl(nn+1,ll+1) = g_bulk + g_boundary ;
		delta_f_nl(nn+1,ll+1) = -g_boundary ;
	end % l loop
end % n loop
