% ------------------------------------
function f = d_spherical_bessel_y(n,x)
% ------------------------------------
% d/dx y(n,x) : derivative of the spherical Bessel function of the second kind
% 170810 HCN : factor pi/2 bug corrected + more compact
f = sqrt(pi./(2*x)).*(n*bessely(n+1/2,x)./x - bessely(n+3/2,x));