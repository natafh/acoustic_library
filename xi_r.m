% ------------------------
function f = xi_r(l,k,B,r) 
% ------------------------
% radial displacement function xi_r = dR(r)/dr (see Triana et al, 2014)
% 	l : angular degree
%	k : mode wave number
%	B : ratio of y_l to j_l Bessel functions for this mode
%	r : vector of dimensionless radius
% 170901 HCN corrige bug : dj_n(k*r)/dr = k*dj_n(u)/du en u=k*r
% 171024 HCN : added separate treatment of case B=0
if (B==0)
	f = k*d_spherical_bessel_j(l,k*r);
else
	f = k*d_spherical_bessel_j(l,k*r) + B * k*d_spherical_bessel_y(l,k*r);
end