% ---------------------------------------------
function [Int, errbnd] = Int_nlm(l,k,B,r_i,r_o) 
% ---------------------------------------------
% Normalization integral I_{nlm} = int_{r_i}^{r_o} i_nlm (see Triana et al, 2014)
% 	l : angular degree
%	k : mode wave number
%	B : ratio of y_l to j_l Bessel functions for this mode
%	r_i : dimensionless lower limit of the integral
%	r_o : dimensionless upper limit of the integral
%
% 190730 HCN : slight rewriting replacing i_nlm function
% 180917 HCN : suppress 'r' dumy variable from inputs
%
% adaptative Gauss-Kronrod quadrature
%	r = vector of dimensionless radius
%   errbnd estimates an upper bound of the absolute error
F = @(r) (r.^2).*(xi_r(l,k,B,r).^2+l*(l+1)*xi_h(l,k,B,r).^2);
[Int, errbnd] = quadgk(F,r_i,r_o);