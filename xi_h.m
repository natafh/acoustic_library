% ------------------------
function f = xi_h(l,k,B,r) 
% ------------------------
% horizontal displacement function xi_h = R(r)/r (see Triana et al, 2014)
% 	l : angular degree
%	k : mode wave number
%	B : ratio of y_l to j_l Bessel functions for this mode
%	r : vector of dimensionless radius
% 190408 HCN : solve NaN for r=0
% 171024 HCN : added separate treatment of case B=0
if (B==0)
	f = spherical_bessel_j(l,k*r)./r ;
else
	f = (spherical_bessel_j(l,k*r) + B * spherical_bessel_y(l,k*r))./r ;
end
%
is_nul = find(r==0); % check if zero present in vector r
if ~isempty(is_nul) % r=0 found
    f(is_nul) = 0; % xi_h = 0 for r=0 (I presume)
end