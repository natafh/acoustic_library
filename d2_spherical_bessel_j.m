% -------------------------------------
function f = d2_spherical_bessel_j(n,x)
% -------------------------------------
% second derivative of the spherical Bessel function of the first kind
% d2/dx2 j(n,x)
% 190408 HCN : solve NaN for x=0 by asymptotic expression (Wolfram Alpha)
% 170810 HCN
f = sqrt(pi./(2*x.^3)).*(besselj(n+1/2,x).*(n*(n-1)./x - x) + 2*besselj(n+3/2,x));
%
is_nul = find(x==0); % check if zero present in vector x
if ~isempty(is_nul) % x=0 found
    if (n==0)
        f(is_nul) = -1./3.; % d2j_0/dx2(0) = -1/3 (Wolfram Alpha)
    elseif (n==2)
        f(is_nul) = 2./15.; % d2j_2/dx2(0) = 2/15 (Wolfram Alpha)
    else
        f(is_nul) = 0; % d2j_n/dx2(0) = 0 for n=1 and n>2 (Wolfram Alpha)
    end
end