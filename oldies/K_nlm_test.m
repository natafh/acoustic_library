% *** version pour test ***
% ------------------------------------------------
%function Knlm = K_nlm(n,l,m,kk,BB,r_i,rayon,theta)
% ------------------------------------------------
% rotational acoustic kernels for a spherical shell (see Triana et al, 2014)
%   n : mode radial index
%	l : mode angular degree
%	m : mode azimuthal order
%	r_i : dimensionless inner sphere radius (used to compute I_nlm)
%   rayon : <Nr x 1> column vector of dimensionless radius from inner to outer shell 
%	theta : <1 x Nt> line vector of colatitudes
%   kk : <n_max x l_max> matrix of mode wavenumbers (m-degenerate)
%   BB : <n_max x l_max> matrix of mode B ratios (m-degenerate)
%
%	Knlm : <Nr x Nt> matrix of kernel K_{nlm}
%
% 171019 HCN from Santiago's original kernel2.m
% 180917 HCN : suppress rayon from input arguments of Int_nlm
% *** NB : NaN present ***
%
% get (n,l) mode data (they are all m-degenerate)
k1=kk(n+1,l+1);
B1=BB(n+1,l+1);
%
% setup the output matrix
Nt = length(theta);
Nr = length(rayon);
Knlm = zeros(Nr,Nt);

% Normalization I_{nlm} in the fluid domain (returns error bound errbnd)
[I_nlm, errbnd] = Int_nlm(l,k1,B1,r_i,1);

% Displacement functions
xir = xi_r(l,k1,B1,rayon);
xih = xi_h(l,k1,B1,rayon);

% Now down to business calculating the kernel
ss = sin(theta);
cc = cos(theta);
p = P_lm(l,m,theta);
q = Q_lm(l,m,theta);

% <Nr x Nt> matrix terms inside the braces :    
term1 = xir.^2 * p.^2;
term2 = xih.^2 * (q.^2 + m^2*p.^2./ss.^2 - 2*p.*q.*cc./ss);
term3 = -2*xir.*xih * p.^2;
% <Nr x Nt> kernel matrix
Knlm = (rayon * ss) .* (term1 + term2 + term3) / I_nlm;