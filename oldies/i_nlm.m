% -------------------------
function f = i_nlm(l,k,B,r) 
% -------------------------
% integrand of normalization integral I_{nlm} (see Triana et al, 2014)
% 	l : angular degree
%	k : mode wave number
%	B : ratio of y_l to j_l Bessel functions for this mode
%	r : vector of dimensionless radius
f = (r.^2).*(xi_r(l,k,B,r).^2+l*(l+1)*xi_h(l,k,B,r).^2);