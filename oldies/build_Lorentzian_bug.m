% ---------------------------------------------------------------------
function [ideb,ifin,spec] = build_Lorentzian(a_N,g_N,ff,f_min,f_max,df)
% ---------------------------------------------------------------------
% builds a Lorentzian spectrum for resonance at frequency ff
% computed between ff - fact*g_N and ff + fact*g_N
%
% inputs
%	a_N : amplitude of ?
%	g_N : (Hz) half-width of the resonance
%	ff : (Hz) resonance peak frequency
%	f_min : (Hz) minimum frequency to be considered
%	f_max : (Hz) maximum frequency to be considered
%	df : (Hz) frequency step
%
% outputs
%	ideb : start index of Lorentzian in (f_min:df:f_max) vector 
%	ifin : end index of Lorentzian in (f_min:df:f_max) vector 
%	spec : (1:ifin-ideb+1) vector of Lorentzian spectrum
%
freq_width = 5000*g_N; % (Hz) frequency width for computing the Lorentzian spectrum
fs_min = max(f_min,ff-freq_width);
fs_max = min(f_max,ff+freq_width);
freq_span = (fs_min:df:fs_max); % (Hz) vector of frequencies 
spec = abs(i*a_N./(g_N+i*(freq_span - ff))); % Lorentzian abs(pressure) spectrum
ideb = round((fs_min-f_min)/df + 1);
ifin = length(freq_span)+ideb-1;