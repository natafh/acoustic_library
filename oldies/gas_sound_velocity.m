function csound = gas_sound_velocity(gas_name, temperature)
% small function to get the speed of sound csound (m/s)
% of gas gas_name at temperature temperature (°C)
%
switch gas_name
% air : http://www.sengpielaudio.com/calculator-speedsound.htm
    case 'air'
        csound = 331.3 + 0.6 * temperature;
    otherwise
        disp(['*** no sound velocity vs temperature relation available in gas_sound_velocity.m for gas ' gas_name])
end