% --------------------------
function f = Q_lm(l,m,theta) 
% --------------------------
% d/dtheta P_lm : (see Triana et al, 2014)
% theta derivative of normalized associated Legendre polynomial P_lm(cos(theta))
% formula from Wolfram, modified for fully normalized Legendre
%	l : degree
%	m : order
%	theta : vector of colatitudes
%
% kept as Q_lm for compatibility
%
f = d_P_lm(l,m,theta);