function [csound, rho_f, nu_f, kappa_f, Gruneisen, Prandtl, nu_b] = gas_properties(gas_name, temperature, pressure)
% function providing thermodynamic properties of gases for acoustics
% as a function of temperature and pressure
% NB : *** add variation of mu_f, k_f and C_P with T and P ? ***
%
% 210811 HCN : add nitrogen
% 190123 HCN : add bulk viscosity following Cramer (2012)
% 190116 HCN
%
% inputs :
%       gas_name : 'air', 'argon', etc
%       temperature : (deg C) temperature
%       pressure : (Pa) pressure
%   
% outputs :
%       csound : (m/s)
%       rho_f : (kg/m3) density
%       nu_f : (m2/s) kinematic viscosity
%       kappa_f : (m2/s) kinematic viscosity
%       Gruneisen : Gruneisen parameter = C_P/C_V or adiabatic index
%       Prandtl : Prandtl number = nu_f /kappa_f
%       nu_b : (m2/s) kinematic bulk viscosity

%
R = 8.314510; % (J mol-1 K-1) universal gas constant
zero_Celsius = 273.15; % (K) absolute temperature of 0°C
%
% thermodynamics properties from 'Air Liquide' encyclopedia
% https://encyclopedia.airliquide.com/fr/air
% also see for air : http://www.sengpielaudio.com/calculator-speedsound.htm
%
% The bulk viscosity is taken from Cramer (2012) and is not always precise
%
switch gas_name
    case 'air'
        M = 0.02896; % (kg mol-1) molar mass of dry air
		Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V or adiabatic index
        C_P = 1.0063e3; % (J kg-1 K-1) specific thermal capacity at 20 C
        mu_f = 1.820e-5; % (Pa s) dynamic viscosity at 20 C
        k_f = 25.9e-3; % (W m-1 K-1) thermal conductivity at 20 C
        mu_bulk_ratio = 1.; % ratio mu_bulk/mu from Cramer (2012)
%        csound = 331.3 + 0.6 * temperature;
    case 'air-COMSOL' % for comparison : at T=10 C and P = 101325 Pa (1 atm)
        temperature = 10.; % (Celsius)
        pressure = 101325.; % (Pa)
        disp(['*** pressure and temperature reset for ' gas_name ':'])
        disp(['P = ' num2str(pressure) ' Pa, T = ' num2str(temperature) ' C'])
        M = 0.02896; % (kg mol-1) molar mass of dry air
		Gruneisen = 1.4; % Gruneisen parameter = C_P/C_V or adiabatic index
        C_P = 1.00542e3; % (J kg-1 K-1) specific thermal capacity at 20 C
        mu_f = 1.814e-5; % (Pa s) dynamic viscosity at 20 C
        k_f = 25.768e-3; % (W m-1 K-1) thermal conductivity at 20 C
%        csound = 343.194; % (m/s) sound velocity
        csound = 343.; % (m/s) sound velocity
        rho_f = 1.204; % (kg m-3) density
        mu_bulk_ratio = 0.6; % COMSOL notice
    case {'N2','azote','nitrogen'}
        M = 0.028013; % (kg mol-1) molar mass of nitrogen
		Gruneisen = 1.4013; % Gruneisen parameter = C_P/C_V or adiabatic index at 25 C
        C_P = 1.0414e3; % (J kg-1 K-1) specific thermal capacity at 25 C
        mu_f = 1.780e-5; % (Pa s) dynamic viscosity at 25 C
        k_f = 25.835e-3; % (W m-1 K-1) thermal conductivity at 25 C
        mu_bulk_ratio = 0.73; % ratio mu_bulk/mu from Cramer (2012)
    case {'argon','Ar'}
        M = 0.039948; % (kg mol-1) molar mass of argon
		Gruneisen = 1.6697; % Gruneisen parameter=C_P/C_V or adiabatic index
        C_P = 521.6; % (J kg-1 K-1) specific thermal capacity at 20 C
        mu_f = 2.23e-5; % (Pa s) dynamic viscosity at 20 C
        k_f = 17.5e-3; % (W m-1 K-1) thermal conductivity at 20 C
        mu_bulk_ratio = 0.; % ratio mu_bulk/mu from Cramer (2012) (zero bulk viscosity for monoatomic gases)
    case {'carbon dioxide','dioxyde de carbone','CO2','CO_2'}
        M = 0.04401; % (kg mol-1) molar mass of carbon dioxide
		Gruneisen = 1.297; % Gruneisen parameter=C_P/C_V or adiabatic index
        C_P = 846.0; % (J kg-1 K-1) specific thermal capacity at 20 C
        mu_f = 1.47e-5; % (Pa s) dynamic viscosity at 20 C
        k_f = 16.2e-3; % (W m-1 K-1) thermal conductivity at 20 C
        mu_bulk_ratio = 4000.; % ratio mu_bulk/mu from Cramer (2012) at ~20C
    case {'sulfur hexafluoride','hexafluorure de soufre','SF6','SF_6'}
        M = 0.146055; % (kg mol-1) molar mass of sulfur hexafluoride
		Gruneisen = 1.100; % Gruneisen parameter=C_P/C_V or adiabatic index
        C_P = 661.0; % (J kg-1 K-1) specific thermal capacity at 20 C
        mu_f = 1.486e-5; % (Pa s) dynamic viscosity at 20 C
        k_f = 13.1e-3; % (W m-1 K-1) thermal conductivity at 20 C
        mu_bulk_ratio = 360.; % ratio mu_bulk/mu from Cramer (2012) at ~20C
    case {'helium','He'}
        M = 0.004003; % (kg mol-1) molar mass of helium
		Gruneisen = 1.6665; % Gruneisen parameter=C_P/C_V or adiabatic index
        C_P = 519.3; % (J kg-1 K-1) specific thermal capacity at 20 C
        mu_f = 1.96e-5; % (Pa s) dynamic viscosity at 20 C
        k_f = 155.5e-3; % (W m-1 K-1) thermal conductivity at 20 C
        mu_bulk_ratio = 0.; % ratio mu_bulk/mu from Cramer (2012) (zero bulk viscosity for monoatomic gases)
    otherwise
        disp(['*** ' gas_name ' not documented in gas_properties.m ***'])
end
%
% needed derived quantities:
% ------------------------------------------------
switch gas_name
    case 'air-COMSOL' % skip csound and rho_f computation
    otherwise
% sound speed for ideal gases (does not depend upon pressure)
        csound = sqrt(Gruneisen * R * (temperature + zero_Celsius) ./ M); % (m/s)
% density for ideal gases (depends upon temperature and pressure)
        rho_f = M * pressure / (R * (temperature + zero_Celsius)); % (kg/m3)
%
end
nu_f = mu_f ./ rho_f; % (m2/s) kinematic viscosity
kappa_f = k_f ./ (rho_f * C_P); % (m2/s) thermal diffusivity
Prandtl = nu_f ./ kappa_f; % Prandtl number
nu_b = mu_bulk_ratio * nu_f; % (m2/s) kinematic bulk viscosity 
