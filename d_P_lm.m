% ----------------------------
function f = d_P_lm(l,m,theta) 
% ----------------------------
% d/dtheta P_lm : (called Q_lm in Triana et al, 2014)
% theta derivative of normalized associated Legendre polynomial P_lm(cos(theta))
% formula from Wolfram, modified for fully normalized Legendre
%	l : degree
%	m : order
%	theta : vector of colatitudes
%
% 190408 HCN : solve NaN for theta=0,pi by asymptotic expression (Nath + Wolfram Alpha)
%
if (m>l || m<0) % not allowed
    disp(['*** error in d_P_lm : l=' int2str(l) ', m=' int2str(m)])
    return
end
%
cc = cos(theta);
ss = sin(theta);
if (l==0)
	f = 0*cc; % P_0^0 = 1 for all theta
else
	if (abs(m)==l)
		f = l*cc.*P_lm(l,m,theta)./ss; % only the first term when m=l
	else
% normalization factor needed for fully normalized Legendre polynomials
		corr =sqrt((l+0.5)/(l-0.5) * (l-m)./(l+m));
		f = (l*cc.*P_lm(l,m,theta) - (l+m)*corr*P_lm(l-1,m,theta))./ss;
	end
end
%
is_nul = find(theta==0); % check if zero present in vector theta
if ~isempty(is_nul) % theta=0 found
    if (m==1)
        f(is_nul) = 0.5*sqrt(l*(l+0.5)*(l+1)); % non zero for m=1
    else
        f(is_nul) = 0; % zero for m~=1
    end
end
%
is_nul = find(theta==pi); % check if pi present in vector theta
if ~isempty(is_nul) % theta=pi found
    if (m==1)
        f(is_nul) = -0.5*sqrt(l*(l+0.5)*(l+1)); % non zero for m=1
    else
        f(is_nul) = 0; % zero for m~=1
    end
end
