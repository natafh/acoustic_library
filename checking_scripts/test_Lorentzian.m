% small script to plot a Lorentzian resonance
% inspired from book "Physical acoustics and metrology of fluids"
% by JPM Trusler, p. 57
%
f_N = 530; % (Hz) frequency of the resonance
delta_f = (-100:0.1:100); % (Hz) range of frequencies around f_N
%
g_N = 2.; % (Hz) half-width of the resonance *** test
a_N = 1; % (Pa Hz) amplitude of what ? *** test
%
p_a = i*a_N./(g_N+i*(f_N+delta_f-f_N));
%
figure('Name','amplitude of Lorentzian resonance')
plot(f_N+delta_f, abs(p_a))
xlabel('frequency (Hz)')
ylabel('modulus of pressure amplitude (Pa)')
title('Lorentzian resonance')
%
figure('Name','log amplitude of Lorentzian resonance')
semilogy(f_N+delta_f, abs(p_a))
xlabel('frequency (Hz)')
ylabel('modulus of pressure amplitude (Pa)')
title('Lorentzian resonance')
%
figure('Name','phase of Lorentzian resonance')
plot(f_N+delta_f, angle(p_a))
xlabel('frequency (Hz)')
ylabel('phase of pressure amplitude (Pa)')
title('Lorentzian resonance')
