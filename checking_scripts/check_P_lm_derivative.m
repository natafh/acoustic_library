% small script to check the theta derivative of P_lm
%
[acoustic_dir, octave] = get_acoustic_dir;
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
%
delta = pi/100.;
theta = 0.:delta:pi; % colatitude
%
l = 6;
m = 4;
%
Plm = P_lm(l,m,theta);
Qlm = d_P_lm(l,m,theta);
%
dPlm_dtheta = (Plm - circshift(Plm,[0 1]))/delta; % calcul derivee directe
%
figure('name',['l= ' num2str(l) ' m= ' num2str(m)])
plot(theta,Qlm,'*b','DisplayName','Q_{lm}')
hold on
plot(theta,dPlm_dtheta,'+r','DisplayName','num')
legend('location','best')
