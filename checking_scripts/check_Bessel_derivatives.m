% check_Bessel_derivatives
% -------------------------------
% to check the derivatives of the spherical Bessel function
%
[acoustic_dir, octave] = get_acoustic_dir;
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
%
nx = 1001;
x=((1:nx)-1)/(nx-1);
n=5
j_n = spherical_bessel_j(n,x);
d_num_j_n = (j_n - circshift(j_n,[0,1]))*(nx-1);
d_num_j_n(1) = NaN;
d_j_n = d_spherical_bessel_j(n, x);
%
figure('Name','dj_n/dz')
plot(x,j_n,'g','DisplayName','j_n')
hold on
plot(x,d_j_n,'*b','DisplayName','dj_n an')
plot(x,d_num_j_n,'or','DisplayName','dj_n num')
legend

d2_num_j_n = (d_j_n - circshift(d_j_n,[0,1]))*(nx-1);
d2_num_j_n(1) = NaN;
d2_j_n = d2_spherical_bessel_j(n, x);
%
figure('Name','d2j_n/dz2')
plot(x,d2_j_n,'.k','DisplayName','d2j_n an')
hold on
plot(x,d2_num_j_n,'or','DisplayName','d2j_n num')

legend
%
% NB: spherical Bessel functions and derivatives in acoustic_library folder
% =========================================================================% -----------------------------------
%function f=d2_spherical_bessel_j(n, x)
% -----------------------------------
% d2/dx2 j(n,x)
% 170810 HCN
%	f = sqrt(pi./(2*x.^3)).*(besselj(n+1/2,x).*(n*(n-1)./x - x) + 2*besselj(n+3/2,x));
%end
% -----------------------------------
%function f=d2_spherical_bessel_y(n, x)
% -----------------------------------
% d2/dx2 y(n,x)
% 170810 HCN
%	f = sqrt(pi./(2*x.^3)).*(bessely(n+1/2,x).*(n*(n-1)./x - x) + 2*bessely(n+3/2,x));
%end
% -----------------------------------
%function f=d_spherical_bessel_j(n, x)
% -----------------------------------
% d/dx j(n,x)
% 170810 HCN : factor pi/2 bug corrected + more compact
%	f = sqrt(pi./(2*x)).*(n*besselj(n+1/2,x)./x - besselj(n+3/2,x));
%end
% -----------------------------------
%function f=d_spherical_bessel_y(n, x)
% -----------------------------------
% d/dx y(n,x)
% 170810 HCN : factor pi/2 bug corrected + more compact
%	f = sqrt(pi./(2*x)).*(n*bessely(n+1/2,x)./x - bessely(n+3/2,x));
%end
% ---------------------------------
%function f=spherical_bessel_j(n, x)
% ---------------------------------
% j(n,x) = sqrt(2/pi x) * J(n + 1/2, x)
%	f = sqrt(pi/2)*besselj(n+1/2,x)./sqrt(x);
%end
% ---------------------------------
%function f=spherical_bessel_y(n, x)
% ---------------------------------
% y(n,x) = sqrt(2/pi x) * Y(n + 1/2, x)
%	f = sqrt(pi/2)*bessely(n+1/2,x)./sqrt(x);
%end
%% ================== =============================== =================== %
