% small script to compare the various expressions for 
% a resonance spectral peak
[acoustic_dir, octave] = get_acoustic_dir;
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
% defauts pour les figures
set(0,'DefaultFigureColor','w')
set(0,'DefaultAxesFontSize',20,'DefaultAxesFontName','Times')
set(0,'DefaultTextFontName','Times','DefaultTextFontSize',20,'DefaultTextFontWeight','demi','DefaultTextInterpreter','tex')
%
a_N = 1.;
g_N = 1.;
ff = 1000.;
%
f_min = ff - 100.;
f_max = ff + 100.;
df=0.1;
freq=(f_min:df:f_max);
%
figure
% Taylored Lorentzian expression (3.3.11) of Trusler (1991)
[ideb,ifin,spec_1] = build_Lorentzian(a_N,g_N,ff,freq);
semilogy(freq(ideb:ifin),spec_1,'DisplayName','Taylor','LineWidth',2)
max(spec_1)
hold on
% full expression (3.3.9) of Trusler (1991)
[ideb,ifin,spec_2] = build_full_Lorentzian(a_N,g_N,ff,freq);
max(spec_2)
semilogy(freq(ideb:ifin),spec_2,'DisplayName','full','LineWidth',2)
% standard expression from Wolfram
[ideb,ifin,spec_3] = build_standard_Lorentzian(a_N,g_N,ff,freq);
max(spec_3)
semilogy(freq(ideb:ifin),spec_3,'DisplayName','standard','LineWidth',2)
%
plot(xlim,[a_N/g_N/sqrt(2) a_N/g_N/sqrt(2)],'k','DisplayName','max/\sqrt{2}')
legend
box on
grid
xlabel('Frequency (Hz)')
ylabel('Amplitude')
title(['Comparison between several formulations of spectral peak ' char(10) 'f_N = ' num2str(ff) ' Hz - g_N = ' num2str(g_N) ' Hz - a_N = ' num2str(a_N)])
