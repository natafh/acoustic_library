% -----------------------------------------------------------
function [ideb,ifin,spec] = build_Lorentzian(a_N,g_N,ff,freq)
% -----------------------------------------------------------
% builds the spectrum for resonance at frequency ff
% computed between ff - fact*g_N and ff + fact*g_N
%
% 210324 HCN : plug in the non-Taylorized expression (eq 3.3.9 of Trusler (1991))
% 190624 HCN : correct bug yielding a shift with the frequency grid
% 190618 HCN : new non-Taylorized definition (from Wikipedia)
%
% inputs
%	a_N : amplitude of ?
%	g_N : (Hz) half-width of the resonance
%	ff : (Hz) resonance peak frequency
%	freq : (Hz) vector of frequencies
%
% outputs
%	ideb : start index of Lorentzian in (f_min:df:f_max) vector 
%	ifin : end index of Lorentzian in (f_min:df:f_max) vector 
%	spec : (1:ifin-ideb+1) vector of Lorentzian spectrum
%
freq_width = 5000*g_N; % (Hz) frequency width for computing the Lorentzian spectrum
df = (freq(end)-freq(1))./(length(freq)-1); % (Hz) frequency increment
% index of first frequency for computation (>=1)
ideb = max(1, round((ff-freq_width-freq(1))./df +1));
% index of last frequency for computation (<=end)
ifin = min(length(freq), round((ff+freq_width-freq(1))./df +1));
n_win = ifin-ideb+1;
%amp = a_N.*2./g_N/pi;
%spec = amp./(1+((freq_span - ff)./(g_N./2.)).^2); % non-Taylorized Lorentzian
 % full expression (eq 3.3.9 of Trusler (1991))
amp = 2*a_N/ff;
spec(1:n_win) = abs(amp./(((ff+i*g_N)./freq(ideb:ifin)).^2- 1));