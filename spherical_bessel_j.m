% ----------------------------------
function f = spherical_bessel_j(n,x)
% ----------------------------------
% spherical Bessel function of the first kind
% j(n,x) = sqrt(2/pi x) * J(n + 1/2, x)
% f is a vector if x is a vector
% 190408 HCN : solve NaN for x=0 by asymptotic expression (Wolfram Alpha)
%
f = sqrt(pi/2)*besselj(n+1/2,x)./sqrt(x);
%
is_nul = find(x==0); % check if zero present in vector x
if ~isempty(is_nul) % x=0 found
    if (n==0)
        f(is_nul) = 1; % 0^0 =1
    else
        f(is_nul) = 0; % 0^n = 0
    end
end