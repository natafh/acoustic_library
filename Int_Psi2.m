% ---------------------------------------------
function [Int, errbnd] = Int_Psi2(l,k,B,r_i,r_o) 
% ---------------------------------------------
% Normalization integral I_{nlm} = int_{r_i}^{r_o} Psi^2
% 	l : angular degree
%	k : mode wave number
%	B : ratio of y_l to j_l Bessel functions for this mode
%	r_i : dimensionless lower limit of the integral
%	r_o : dimensionless upper limit of the integral
%
% 191117 HCN : integration of the non-dimensional acoustic eigenfunction
% NB : could be computed analytically (e.g., see Guianvarc'h et al, 2009).
%
% adaptative Gauss-Kronrod quadrature
%	r = vector of dimensionless radius
%   errbnd estimates an upper bound of the absolute error
if (B==0)
    F = @(r) (r.^2).*(spherical_bessel_j(l,k*r)).^2;
else
    F = @(r) (r.^2).*(spherical_bessel_j(l,k*r) + B * spherical_bessel_y(l,k*r)).^2;
end
[Int, errbnd] = quadgk(F,r_i,r_o);