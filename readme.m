% example script to plot an eigenmode amplitude and kernel
% of a spherical shell from modes computed with compute_modes_menu
%
filename = input(['enter the sphere type of the modes file you want to plot (ex: ZoRo-rigid-air)' char(10)],'s');
%
% read modes file and associated data
[parameters,n_max,l_max,kk,ff,BB,C_nl,Acc,gamma_nl,dk2_nlm,g_nl,delta_f_nl, delta_f_shell,ReadMe_elastic_modes] = read_acoustic_modes(filename);
%
r_i = parameters.rf(1); % radius of the inner sphere (0 if none)
r_o = parameters.rf(2); % radius of the outer sphere
% set up the grid
rayon = transpose(linspace(r_i./r_o,1,101));
theta = linspace(0,pi,181);
x = rayon*sin(theta);
y = rayon*cos(theta);

% select desired mode
n = input(['enter radial number n (0<= n <=' int2str(n_max) ')' char(10)]);
l = input(['enter angular degree l (0<= l <=' int2str(l_max) ')' char(10)]);
m = input(['enter azimuthal order m (0<= m <=' int2str(l) ')' char(10)]);
mode_name = ['_{' int2str(n) '}S_{' int2str(l) '}^{' int2str(m) '}'];

% compute amplitude and plot
amplitude = A_nlm(n,l,m,kk,BB,rayon,theta);
cmax = max(abs(min(min(amplitude))),abs(max(max(amplitude))));
figure('Name','mode amplitude','Position', [200 300 400 600])
pcolor(x,y,amplitude); shading flat; axis equal tight off;
caxis([-cmax cmax]); colormap('jet'); colorbar
title({filename ; [mode_name ' amplitude']})

% compute kernel and plot
krnl = K_nlm(n,l,m,kk,BB,r_i./r_o,rayon,theta);
figure('Name','mode kernel','Position', [600 300 400 600])
pcolor(x,y,krnl); shading flat; axis equal tight off;
colormap('hot'); colorbar
title({filename ; [mode_name ' kernel']})