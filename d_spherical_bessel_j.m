% ------------------------------------
function f = d_spherical_bessel_j(n,x)
% ------------------------------------
% d/dx j(n,x) : derivative of the spherical Bessel function of the first kind
% 190408 HCN : solve NaN for x=0 by asymptotic expression (Wolfram Alpha)
% 170810 HCN : factor pi/2 bug corrected + more compact
%
f = sqrt(pi./(2*x)).*(n*besselj(n+1/2,x)./x - besselj(n+3/2,x));
%
is_nul = find(x==0); % check if zero present in vector x
if ~isempty(is_nul) % x=0 found
    if (n==1)
        f(is_nul) = 1./3.; % dj_1/dx(0) = 1/3 (Wolfram Alpha)
    else
        f(is_nul) = 0; % dj_n/dx(0) = 0 for n=0 and n>1 (Wolfram Alpha)
    end
end