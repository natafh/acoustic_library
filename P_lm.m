% --------------------------
function f = P_lm(l,m,theta) 
% --------------------------
% normalized associated Legendre polynomial P_lm(cos(theta))
%	l : degree
%	m : order
%	theta : vector of colatitudes
%
if (m>l || m<0) % not allowed
    disp(['*** error in P_lm : l=' int2str(l) ', m=' int2str(m)])
    return
end
%
x = cos(theta);
truc = legendre(l,x,'norm'); % all m returned
f = truc(m+1,:);