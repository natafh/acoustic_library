% --------------------------------------------
function Anlm = A_nlm(n,l,m,kk,BB,rayon,theta)
% --------------------------------------------
% Spherical Shell Pressure Eigenmode Amplitude (see Triana et al, 2014)
%   n : mode radial index
%	l : mode angular degree
%	m : mode azimuthal order
%   rayon : <Nr x 1> column vector of dimensionless radius from inner to outer shell 
%	theta : <1 x Nt> line vector of colatitudes
%   kk : <n_max x l_max> matrix of mode wavenumbers (m-degenerate)
%   BB : <n_max x l_max> matrix of mode B ratios (m-degenerate)
%
%	Anlm : <Nr x Nt> matrix of mode pressure A_{nlm}
%
% 171019 HCN from Santiago's original A_nlm.m
%
% get (n,l) mode data (they are all m-degenerate)
k1=kk(n+1,l+1);
B1=BB(n+1,l+1);
%
% setup the output matrix
Nt = length(theta);
Nr = length(rayon);
Knlm = zeros(Nr,Nt);

% the radial eigenfunction is:
if (B1==0)
    radial = spherical_bessel_j(l,k1*rayon);
else
    radial = spherical_bessel_j(l,k1*rayon) + B1*spherical_bessel_y(l,k1*rayon);
end

Anlm = radial * P_lm(l,m,theta); % <Nr x Nt> matrix of pressure mode amplitude